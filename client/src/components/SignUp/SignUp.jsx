import React, { useState } from 'react';
import { TextField, Button, Grid, Typography, Box, InputAdornment } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import api from '../../services/api';
import backgroundImage from '../../assets/signin-bg.jpeg';
import "@fontsource/montserrat/700.css";
import EmailIcon from '@mui/icons-material/Email';
import HttpsIcon from '@mui/icons-material/Https';
import PersonIcon from '@mui/icons-material/Person';
import './SignUp.css'

const SignUp = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        username: '',
        email: '',
        password: '',
    });

    const [error, setError] = useState('');


    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await api.post('/user/register', formData);
            navigate('/signin');
        } catch (error) {
            console.error('Error signing up:', error);
            setError('This email is already Registered');
        }
    };

    const handleSignInClick = () => {
        navigate('/signin');
    };

    return (
        <div className='signup-main'>
            <Grid container >
                <Grid item sm={5} className='signup-grid' style={{ backgroundImage: `url(${backgroundImage})` }}>
                    <Grid item sm={7} >
                        <Box className='signup-left' >
                            <Typography component="h1" variant="h5" fontFamily={"montserrat "} className='signin-welcome'>
                                Welcome Back!
                            </Typography>
                            <Typography variant="body1" className='signin-heading-left'>
                                To keep connected with us please login with your personal info
                            </Typography>
                            <Button variant="outlined" color="inherit" className='signin-button' onClick={handleSignInClick}>Sign In</Button>
                        </Box>
                    </Grid>
                </Grid>
                <Grid item sm={7} >
                    <Box className='signup-right'>
                        <Grid item sm={6} >
                            <Typography component="h1" variant="h5" className='signup-heading' fontFamily={"montserrat "}>
                                Create Account
                            </Typography>
                            <form onSubmit={handleSubmit} >
                                <TextField variant="outlined" margin="normal" required fullWidth id="username" name="username" value={formData.username} onChange={handleChange} autoFocus 
                                    InputProps={{ startAdornment: (<InputAdornment position="start">  <PersonIcon /> </InputAdornment>), }}
                                    InputLabelProps={{ shrink: !!formData.username, }}
                                    label={!formData.username ? <span className='signup-label'>Name</span> : ''} />

                                <TextField variant="outlined" margin="normal" required fullWidth id="email" name="email" value={formData.email} onChange={handleChange} autoComplete="email" autoFocus type='email'
                                    InputProps={{ startAdornment: (<InputAdornment position="start">  <EmailIcon /> </InputAdornment>), }}
                                    InputLabelProps={{ shrink: !!formData.email, }}
                                    label={!formData.email ? <span className='signup-label'>Email</span> : ''} />

                                <TextField variant="outlined" margin="normal" required fullWidth name="password" value={formData.password} onChange={handleChange} type="password" id="password" autoFocus autoComplete="current-password"
                                    InputProps={{ startAdornment: (<InputAdornment position="start">  <HttpsIcon /> </InputAdornment>), }}
                                    InputLabelProps={{ shrink: !!formData.password, }}
                                    label={!formData.password ? <span className='signup-label'>Password</span> : ''} />
                                {error && <Typography color="error">{error}</Typography>}
                                <Button type="submit" variant="contained" fullWidth className='signup-button'>
                                    Sign UP
                                </Button>
                            </form>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
};

export default SignUp;