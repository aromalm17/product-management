import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, TextField, Button, Grid, MenuItem, InputAdornment } from '@mui/material';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import api from '../../services/api';

const UpdateProductDialog = ({ open, onClose }) => {
    const [name, setName] = useState('');
    const [ram, setRAM] = useState('');
    const [price, setPrice] = useState(0);
    const [storage, setStorage] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [subCategory, setSubCategory] = useState('');
    const [subCategoriesList, setSubCategoriesList] = useState([]);
    const [description, setDescription] = useState('');
    const [images, setImages] = useState([]);
    const [code, setCode] = useState('');
    const [partNumber, setPartNumber] = useState('');
    const { id } = useParams();

    const token = localStorage.getItem('token');

    useEffect(() => {
        if (open) {
            const fetchProduct = async () => {
                try {
                    const response = await api.get(`/product/${id}`);
                    const product = response.data;

                    setName(product.name);
                    setRAM(product.ram);
                    setPrice(product.price);
                    setStorage(product.storage);
                    setQuantity(product.quantity);
                    setDescription(product.description);
                    setCode(product.code);
                    setPartNumber(product.partNumber);
                    setImages(product.images || []);
                    setSubCategory(product.subCategory._id);

                    fetchSubCategories();
                } catch (error) {
                    console.error('Error fetching product:', error);
                }
            };
            fetchProduct();
        }
    }, [id, open]);

    const fetchSubCategories = async () => {
        try {
            const response = await api.get('/subcategory/view');
            setSubCategoriesList(response.data);
        } catch (error) {
            console.error('Error fetching subcategories:', error);
        }
    };

    const handleImageUpload = (event) => {
        const files = Array.from(event.target.files);
        setImages((prevImages) => [
            ...prevImages,
            ...files.map((file) => ({
                file,
                url: URL.createObjectURL(file)
            }))
        ]);
    };

    const handleSubmit = async () => {
        if (!name || !price || !ram || !storage || !subCategory || !description) {
            alert('Please fill in all required fields.');
            return;
        }

        try {
            const formData = new FormData();
            formData.append('name', name);
            formData.append('ram', ram);
            formData.append('price', price);
            formData.append('storage', storage);
            formData.append('quantity', quantity);
            formData.append('subCategory', subCategory);
            formData.append('description', description);
            formData.append('code', code);
            formData.append('partNumber', partNumber);

            images.forEach((image) => {
                if (image.file) {
                    formData.append('images', image.file);
                }
            });

            await axios.put(`http://localhost:3001/product/update/${id}`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`, 
                }
            });
            onClose();
        } catch (error) {
            console.error('Error updating product:', error);
            alert('There was an error updating the product. Please try again.');
        }
    };

    useEffect(() => {
        return () => {
            images.forEach((image) => {
                if (image.url) {
                    URL.revokeObjectURL(image.url);
                }
            });
        };
    }, [images]);

    return (
        <Dialog open={open} onClose={onClose} fullWidth maxWidth="sm">
            <DialogTitle>Update Product</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField label="Title" fullWidth value={name} onChange={(e) => setName(e.target.value)} />
                    </Grid>
                    <Grid item xs={12} container spacing={2}>
                        <Grid item xs={6}>
                            <TextField label="Code" fullWidth value={code} onChange={(e) => setCode(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Part Number" fullWidth value={partNumber} onChange={(e) => setPartNumber(e.target.value)} />
                        </Grid>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="RAM" fullWidth value={ram} onChange={(e) => setRAM(e.target.value)} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="Price" fullWidth type="number" InputProps={{ startAdornment: <InputAdornment position="start">$</InputAdornment>, }} value={price} onChange={(e) => setPrice(Number(e.target.value))} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="Storage" fullWidth value={storage} onChange={(e) => setStorage(e.target.value)} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="QTY" fullWidth type="number" value={quantity} onChange={(e) => setQuantity(Number(e.target.value))} />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField label="Sub category" fullWidth select value={subCategory} onChange={(e) => setSubCategory(e.target.value)}  >
                            {subCategoriesList.map((subCategoryItem) => (
                                <MenuItem key={subCategoryItem._id} value={subCategoryItem._id}>
                                    {subCategoryItem.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>

                    <Grid item xs={12}>
                        <TextField label="Description" fullWidth multiline rows={3} value={description} onChange={(e) => setDescription(e.target.value)} />
                    </Grid>
                    <Grid item xs={12}>
                        <input accept="image/*" style={{ display: 'none' }} id="upload-image" multiple type="file" onChange={handleImageUpload} />
                        <label htmlFor="upload-image">
                            <Button variant="contained" color="primary" component="span" startIcon={<AddPhotoAlternateIcon />}  > Upload Images </Button>
                        </label>
                        <Grid container spacing={2} style={{ marginTop: 8 }}>
                            {images.map((image, index) => (
                                <Grid item key={index}>
                                    <img
                                        src={image.url || `http://localhost:3001/${image}`}
                                        alt={`Upload ${index}`}
                                        width="100"
                                    />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleSubmit} variant="contained" color="primary"> Update </Button>
                <Button onClick={onClose}>Discard</Button>
            </DialogActions>
        </Dialog>
    );
};

export default UpdateProductDialog;