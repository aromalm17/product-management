import React, { useState, useEffect, useCallback } from 'react';
import { Box, Button, Typography, Grid, Paper, CircularProgress } from '@mui/material';
import Breadcrumbs from '../Breadcrumbs';
import UpdateProductDialog from './UpdateProduct';
import api from '../../services/api';
import { useParams } from 'react-router-dom';
import './product.css'

const ProductDetails = () => {
    const [productDetails, setProductDetails] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);

    const { id } = useParams();

    const fetchProductDetails = useCallback(async () => {
        try {
            setLoading(true);
            const response = await api.get(`http://localhost:3001/product/${id}`);
            setProductDetails(response.data);
        } catch (error) {
            console.error('Error fetching product details:', error);
            setError('Error fetching product details. Please try again later.');
        } finally {
            setLoading(false);
        }
    }, [id]);

    useEffect(() => {
        fetchProductDetails();
    }, [fetchProductDetails]);

    const handleDialogOpen = () => {
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
        fetchProductDetails();
    };

    if (loading) {
        return (
            <Box className='productdtls-loading'>
                <CircularProgress />
            </Box>
        );
    }

    if (error) {
        return (
            <Box className='productdtls-error'>
                <Typography variant="h6" color="error">{error}</Typography>
            </Box>
        );
    }

    return (
        <div className='productdetails-main' >
            <div className='productdtls-brdcmbs'>
                <Breadcrumbs />
            </div>
            <Grid container spacing={4} justifyContent="center">
                <Grid item xs={12} sm={6} md={4}>
                    <Paper className='productdtls-paper' elevation={3}>
                        {productDetails.images && productDetails.images.length > 0 && (
                            <img src={`http://localhost:3001/${productDetails.images[0]}`} alt="Product Main" style={{ maxWidth: '100%', height: '300px' }} />)}
                    </Paper>
                    <Grid container spacing={1} mt={1}>
                        {productDetails.images && productDetails.images.slice(1).map((element, index) => (
                            <Grid item xs={6} key={index}>
                                <Paper elevation={1} className='productdtls-paper'>
                                    <img src={`http://localhost:3001/${element}`} alt={`Product Thumbnail ${index}`} style={{ maxWidth: '100%', height: '120px' }} />
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <Typography my={2} variant="h4">{productDetails.name}</Typography>
                    <Typography variant="h5" color="textSecondary">
                        ${productDetails.price}
                    </Typography>
                    <Typography variant="body1" color="textSecondary">
                        Availability: {productDetails.quantity < 1 ? "Out of Stock" : "In Stock"}
                    </Typography>
                    <Typography variant="body1">Quantity : {productDetails.quantity || "0"}</Typography>
                    <Box my={2}>
                        <Typography variant="body1">RAM : {productDetails.ram || "N/A"}</Typography>
                        <Typography variant="body1">Storage : {productDetails.storage || "N/A"}</Typography>
                        <Typography variant="body1">Description : {productDetails.description || "0"}</Typography>
                    </Box>
                    <Box my={2}>
                        <Typography variant="body1">Code : {productDetails.code || "N/A"}</Typography>
                        <Typography variant="body1">Part Number : {productDetails.partNumber || "N/A"}</Typography>
                    </Box>
                    <Box mt={3}>
                        <Button variant="outlined" color="secondary" onClick={handleDialogOpen}>  Edit product  </Button>
                    </Box>
                </Grid>
            </Grid>
            <UpdateProductDialog open={dialogOpen} onClose={handleDialogClose} productDetails={productDetails} />
        </div>
    );
};

export default ProductDetails;