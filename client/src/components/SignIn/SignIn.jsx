import React, { useState } from 'react';
import api from '../../services/api';
import { Link, useNavigate } from 'react-router-dom';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import backgroundImage from '../../assets/signin-bg.jpeg';
import "@fontsource/montserrat/700.css";
import InputAdornment from '@mui/material/InputAdornment';
import EmailIcon from '@mui/icons-material/Email';
import HttpsIcon from '@mui/icons-material/Https';
import './SignIn.css'

const SignIn = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        email: '',
        password: '',
    });
    const [error, setError] = useState('');

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await api.post('/user/login', formData);
            localStorage.setItem('token', response.data.token);
            navigate('/');
        } catch (error) {
            console.error('Error signing in:', error);
            setError('Invalid email or password.');
        }
    };

    const handleSignInClick = () => {
        navigate('/signup');
    };

    return (
<div className="signInContainer">      
      <Grid container >
                <Grid item sm={7} >
                <Box className='signin-box'>                                 
                           <Grid item sm={6}>
                            <Typography component="h1" variant="h5" className='signin-heading'>
                                Sign In to <br /> Your Account
                            </Typography>
                            <form onSubmit={handleSubmit} >
                                <TextField variant="outlined" margin="normal" required fullWidth id="email" name="email" value={formData.email} onChange={handleChange} type='email' autoComplete="email" autoFocus 
                                    InputProps={{ startAdornment: (<InputAdornment position="start">  <EmailIcon /> </InputAdornment>), }}
                                    InputLabelProps={{ shrink: !!formData.email, }}
                                    label={!formData.email ? <span className='signin-input'>Email</span> : ''} />

                                <TextField variant="outlined" margin="normal" required fullWidth name="password" value={formData.password} onChange={handleChange}  type="password" id="password" autoFocus autoComplete="current-password"
                                    InputProps={{ startAdornment: (<InputAdornment position="start">  <HttpsIcon /> </InputAdornment>), }}
                                    InputLabelProps={{ shrink: !!formData.password, }}
                                    label={!formData.password ? <span className='signin-input'>Password</span> : ''} />
                                <Link href="#" variant="body2" className='forgot-password-link' >
                                    forgot password?
                                </Link><br />
                                <Button type="submit" variant="contained" fullWidth className='signin-btn'>
                                    Sign In
                                </Button>
                                {error && <Typography color="error">{error}</Typography>}
                            </form>
                        </Grid>
                    </Box>
                </Grid>
                <Grid item sm={5} style={{ backgroundImage: `url(${backgroundImage})`}} className='signin-background'>
                    <Grid item sm={7} >
                        <Box className='signin-box-right' >
                            <Typography component="h1" variant="h5" fontFamily={"montserrat "} fontSize={'34px'}>
                                Hello Friend!
                            </Typography>
                            <Typography variant="body1" className='signin-right-heading'>
                                Enter your personal details and <br /> start your journey with us
                            </Typography>
                            <Button variant="outlined" color="inherit" className='signup-btn' onClick={handleSignInClick}>Sign Up</Button>
                        </Box>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
};

export default SignIn;