import React, { useState } from 'react';
import { AppBar, Toolbar, IconButton, Button, Grid, InputBase } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import './Navbar.css';
import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';

const Navbar = ({ onSearch }) => {
    const { logout } = useContext(AuthContext);
    const [searchTerm, setSearchTerm] = useState('');

    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };

    const handleSearchClick = (e) => {
        e.preventDefault();
        onSearch(searchTerm);
    };


    return (
        <AppBar className='appBar'>
            <Toolbar>
                <Grid container alignItems="center" className="searchContainer">
                    <form onSubmit={handleSearchClick}>
                        <InputBase placeholder="Search any things" value={searchTerm} onChange={handleSearchChange} className="searchInput" />
                        <Button type="submit" variant="contained" className="searchButton">  Search  </Button>
                    </form>
                </Grid>
                <div className="flexGrow" />
                <IconButton color="inherit" onClick={logout} aria-label="logout">
                    <LogoutIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;