import React, { useEffect, useState } from 'react';
import { Grid, Card, CardContent, CardMedia, Typography, Button, Pagination, Accordion, AccordionSummary, AccordionDetails, FormControlLabel, Checkbox } from '@mui/material';
import Breadcrumbs from '../Breadcrumbs';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import './home.css';
import AddCategory from './AddCategory';
import AddSubcategory from './AddSubcategory';
import AddProduct from './AddProduct';
import api from '../../services/api';
import NavBar from '../Navbar';
import { useNavigate } from 'react-router-dom';

const Home = () => {
    const [products, setProducts] = useState([]);
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [subcategoryDialogOpen, setSubcategoryDialogOpen] = useState(false);
    const [productDialogOpen, setProductDialogOpen] = useState(false);
    const [categoryName, setCategoryName] = useState('');
    const [subcategoryName, setSubcategoryName] = useState('');
    const [searchTerm, setSearchTerm] = useState('');
    const [subcategories, setSubcategories] = useState([]);
    const [manufacturers, setManufacturers] = useState([]);
    const [selectedManufacturers, setSelectedManufacturers] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        fetchCategories();
    }, []);

    useEffect(() => {
        fetchProducts(page, searchTerm, selectedManufacturers);
    }, [page, searchTerm, selectedManufacturers]);

    const fetchCategories = async () => {
        try {
            const response = await api.get('/subcategory/view');
            setManufacturers(response.data);
        } catch (error) {
            console.error('Error fetching categories:', error);
        }
    };

    const fetchProducts = async (page, searchTerm, selectedManufacturers) => {
        try {
            const response = await api.get('/product', {
                params: {
                    page,
                    search: searchTerm,
                    manufacturers: selectedManufacturers.join(',')
                }
            });
            setProducts(response.data.products);
            setTotalPages(response.data.totalPages);
        } catch (error) {
            console.error('Error fetching products:', error);
        }
    };

    const handleCheckboxChange = (event, manufacturer) => {
        const { checked } = event.target;
        const newSelectedManufacturers = checked
            ? [...selectedManufacturers, manufacturer._id]
            : selectedManufacturers.filter(id => id !== manufacturer._id);
        setSelectedManufacturers(newSelectedManufacturers);
    };

    const handlePageChange = (event, value) => {
        setPage(value);
    };

    const handleOpenDialog = () => {
        setDialogOpen(true);
    };

    const handleCloseDialog = () => {
        setDialogOpen(false);
        setCategoryName('');
        fetchCategories();
    };

    const handleOpenSubcategoryDialog = () => {
        setSubcategoryDialogOpen(true);
    };

    const handleCloseSubcategoryDialog = () => {
        setSubcategoryDialogOpen(false);
        setSubcategoryName('');
        fetchCategories();
    };

    const handleOpenProductDialog = () => {
        setProductDialogOpen(true);
    };

    const handleCloseProductDialog = () => {
        setProductDialogOpen(false);
        fetchCategories();
        setSubcategoryName('');
    };

    const handleAddCategory = () => {
        handleCloseDialog();
    };

    const handleAddSubcategory = () => {
        handleCloseSubcategoryDialog();
    };

    const handleAddProduct = (productData) => {
        console.log('Product added:', productData);
    };

    const handleSearch = (searchTerm) => {
        setSearchTerm(searchTerm);
    };

    const gotoProduct = (id) => {
        navigate(`/product/${id}`);
    };


    const categories = [...new Map(manufacturers.map(item => [item.category._id, item.category])).values()];

    return (
        <>
            <NavBar searchTerm={searchTerm} onSearch={handleSearch} />
            <div className='main-home'>
                <Grid container spacing={2}>
                    <Grid item sm={2} my={4}>
                        <h2>Categories</h2>
                        {categories.map(category => (
                            <Accordion key={category._id}>
                                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls={`panel-${category._id}-content`} id={`panel-${category._id}-header`} >
                                    {category.name}
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div>
                                        {manufacturers
                                            .filter(manufacturer => manufacturer.category._id === category._id)
                                            .map(filteredManufacturer => (
                                                <span key={filteredManufacturer._id}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox checked={selectedManufacturers.includes(filteredManufacturer._id)} onChange={(event) => handleCheckboxChange(event, filteredManufacturer)} />
                                                        }
                                                        label={filteredManufacturer.name}
                                                    />
                                                </span>
                                            ))}
                                    </div>
                                </AccordionDetails>
                            </Accordion>
                        ))}
                    </Grid>
                    <Grid item sm={10}>
                        <div className="container">
                            <Breadcrumbs />
                            <div className="button-group">
                                <Button variant="contained" className="custom-button" onClick={handleOpenDialog}>Add category</Button>
                                <Button variant="contained" className="custom-button" onClick={handleOpenSubcategoryDialog}>Add sub category</Button>
                                <Button variant="contained" className="custom-button" onClick={handleOpenProductDialog}>Add product</Button>
                            </div>
                        </div>
                        <Grid container spacing={2} my={1} className='card-home'>
                            {products.map((product) => (
                                <Grid item key={product.id} lg={3} md={6} sm={12} >
                                    <Card onClick={() => gotoProduct(product._id)} >
                                        <CardMedia component="img" alt={product.name} height="150" image={`http://localhost:3001/${product.images[0]}`} />
                                        <CardContent>
                                            <Typography gutterBottom variant="h6">
                                                {product.name}
                                            </Typography>
                                            <Typography variant="h6" color="text.secondary">
                                                ${product.price}
                                            </Typography>
                                            <Typography variant="body2" >
                                                RAM: {product.ram}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Storage: {product.storage}
                                            </Typography>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                        <Pagination count={totalPages} page={page} onChange={handlePageChange} color="primary" className='pagination-home' />
                    </Grid>
                </Grid>
                <AddCategory open={dialogOpen} onClose={handleCloseDialog} onAdd={handleAddCategory} />
                <AddSubcategory open={subcategoryDialogOpen} onClose={handleCloseSubcategoryDialog} categories={categories} onAdd={handleAddSubcategory} />
                <AddProduct open={productDialogOpen} onClose={handleCloseProductDialog} subcategories={subcategories} onAdd={handleAddProduct} />
            </div>
        </>
    );
};

export default Home;