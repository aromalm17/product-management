import React, { useEffect, useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, TextField, Button, Grid, MenuItem, InputAdornment } from '@mui/material';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import axios from 'axios';
import api from '../../services/api'
import './index.css'

const AddProductDialog = ({ open, onClose }) => {
    const [name, setName] = useState('');
    const [ram, setRAM] = useState('');
    const [price, setPrice] = useState();
    const [storage, setStorage] = useState('');
    const [quantity, setQuantity] = useState(1);
    const [subCategory, setSubCategory] = useState('');
    const [description, setDescription] = useState('');
    const [images, setImages] = useState([]);
    const [code, setCode] = useState('');
    const [partNumber, setPartNumber] = useState('');
    const [subCategoryList, setSubCategoryList] = useState([]);

    const token = localStorage.getItem('token');

    useEffect(() => {
        api.get('/subcategory/view')
            .then(response => {
                setSubCategoryList(response.data); 
            })
            .catch(error => {
                console.error('Error fetching subcategory list:', error);
            });
    }, []);

    const handleImageUpload = (event) => {
        const files = Array.from(event.target.files);
        setImages(files);
    };

    const handleSubmit = async () => {
        try {
            const formData = new FormData();
            formData.append('name', name);
            formData.append('ram', ram);
            formData.append('price', price);
            formData.append('storage', storage);
            formData.append('quantity', quantity);
            formData.append('subCategory', subCategory);
            formData.append('description', description);
            formData.append('code', code);
            formData.append('partNumber', partNumber);

            images.forEach((image, index) => {
                formData.append(`images`, image);
            });

            const response = await axios.post('http://localhost:3001/product/add', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Authorization': `Bearer ${token}`, 
                }
            });
            onClose();
        } catch (error) {
            console.error('Error adding product:', error);
        }
    };

    return (
        <Dialog open={open} onClose={onClose} fullWidth maxWidth="sm">
            <DialogTitle>Add Product</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField label="Title" fullWidth value={name} onChange={(e) => setName(e.target.value)} />
                    </Grid>
                    <Grid item xs={12} className='add-prdt-grid'>
                        <Grid item xs={6}>
                            <TextField label="Code" fullWidth value={code} onChange={(e) => setCode(e.target.value)} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label="Part Number" fullWidth value={partNumber} onChange={(e) => setPartNumber(e.target.value)} />
                        </Grid>
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="RAM" fullWidth value={ram} onChange={(e) => setRAM(e.target.value)} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="Price" fullWidth type="number" InputProps={{ startAdornment: <InputAdornment position="start">$</InputAdornment>, }} value={price} onChange={(e) => setPrice(e.target.value)} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="Storage" fullWidth value={storage} onChange={(e) => setStorage(e.target.value)} />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField label="QTY" fullWidth type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                    </Grid>
               
                    <Grid item xs={12}>
                        <TextField label="Sub category" fullWidth select value={subCategory} onChange={(e) => setSubCategory(e.target.value)}>
                            {subCategoryList.map((subcategory) => (
                                <MenuItem key={subcategory._id} value={subcategory._id}>{subcategory.name}</MenuItem>
                            ))}
                        </TextField>
              
                    </Grid>
                    <Grid item xs={12}>
                        <TextField label="Description" fullWidth multiline rows={3} value={description} onChange={(e) => setDescription(e.target.value)} />
                    </Grid>
                    <Grid item xs={12}>
                        <input accept="image/*" className='add-prdt-inpt' id="upload-image" multiple type="file" onChange={handleImageUpload} />
                        <label htmlFor="upload-image">
                            <Button variant="contained" color="primary" component="span" startIcon={<AddPhotoAlternateIcon />} >  Upload Images  </Button>
                        </label>
                        <Grid container spacing={2} style={{ marginTop: 8 }}>
                            {images.map((image, index) => (
                                <Grid item key={index}>
                                    <img src={URL.createObjectURL(image)} alt={`Upload ${index}`} width="100" />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleSubmit} variant="contained" className="custom-button-add"> Add </Button>
                <Button onClick={onClose} className="custom-button-discard">Discard</Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddProductDialog;