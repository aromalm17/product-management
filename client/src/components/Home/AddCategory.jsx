import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dialog, DialogActions, DialogContent, DialogTitle, Button, TextField } from '@mui/material';
import api from '../../services/api'

const AddCategory = ({ open, onClose }) => {
    const [categoryName, setCategoryName] = useState('');

    const handleClose = () => {
        onClose();
        setCategoryName('');
    };

    const handleAdd = async () => {
        if (!categoryName.trim()) {
            alert('Please enter a valid category name.');
            return;
        }

        try {
            const response = await api.post('/category/add', { name: categoryName });
            handleClose();
        } catch (error) {
            alert('Failed to add category. Please try again.');
        }
    };

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle className='add-ctrgy-dialog'>Add Category</DialogTitle>
            <DialogContent className='add-ctrgy-dialog-cnt'>
                <TextField autoFocus placeholder="Enter category name" multiline margin="dense" type="text" fullWidth value={categoryName} onChange={(e) => setCategoryName(e.target.value)} />
            </DialogContent>
            <DialogActions className='add-ctrgy-dialog-actn'>
                <Button onClick={handleAdd} className="custom-button-add">Add</Button>
                <Button onClick={handleClose} className="custom-button-discard">Discard</Button>
            </DialogActions>
        </Dialog>
    );
};

AddCategory.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default AddCategory;