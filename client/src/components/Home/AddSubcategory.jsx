import React, { useState, useEffect } from 'react';
import { Dialog, DialogActions, DialogContent, DialogTitle, Button, TextField, Select, MenuItem } from '@mui/material';
import api from '../../services/api';

const AddSubcategory = ({ open, onClose }) => {
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState('');
    const [subcategoryName, setSubcategoryName] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchCategories = async () => {
            try {
                const response = await api.get('category/view');
                setCategories(response.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching categories:', error);
                setLoading(false);
            }
        };
    
        fetchCategories(); 
    }, []);
    

    const handleClose = () => {
        onClose();
        setSelectedCategory('');
        setSubcategoryName('');
    };

    const handleAdd = async () => {
        try {
            const response = await api.post('/subcategory/add', {
                category: selectedCategory,
                name: subcategoryName
            });
            handleClose();
        } catch (error) {
            console.error('Error adding subcategory:', error);
        }
    };

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle className='.add-subctrgy-dialog'>Add Subcategory</DialogTitle>
            <DialogContent className='.add-subctrgy-dialog-cnt'>
                {loading ? (
                    <div>Loading categories...</div>
                ) : (
                    <Select value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)} displayEmpty fullWidth>
                        <MenuItem value="" disabled>
                            Select category
                        </MenuItem>
                        {categories.map((category) => (
                            <MenuItem key={category._id} value={category._id}>{category.name}</MenuItem>
                        ))}
                    </Select>
                )}
                <TextField autoFocus placeholder="Enter subcategory name" id="outlined-textarea" multiline margin="dense" type="text" fullWidth value={subcategoryName} onChange={(e) => setSubcategoryName(e.target.value)} style={{ marginTop: '10px' }} />
            </DialogContent>
            <DialogActions className='add-subctrgy-dialog-actn'>
                <Button onClick={handleAdd} className="custom-button-add">Add</Button>
                <Button variant="contained" onClick={handleClose} className="custom-button-discard">Discard</Button>
            </DialogActions>
        </Dialog>
    );
};

export default AddSubcategory;