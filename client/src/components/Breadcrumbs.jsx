import React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';

function CustomSeparator() {
  const location = useLocation();
  const navigate = useNavigate();

  const generateBreadcrumbs = () => {
    if (location.pathname === '/') {
      return [
        <Link underline="hover" key="1" color="inherit" component={RouterLink} to="/" onClick={handleClick}>
          Home
        </Link>,
        <Link>
        </Link>,
      ];
    } else if (location.pathname.startsWith('/product/')) {
      return [
        <Link underline="hover" key="1" color="inherit" component={RouterLink} to="/" onClick={handleClick}>
          Home
        </Link>,
        <Link underline="hover" key="2" color="textPrimary" component={RouterLink} to={location.pathname} onClick={handleClick} aria-current="page">
          Product Details
        </Link>,
      ];
    } else {
      return [
        <Link underline="hover" key="1" color="inherit" component={RouterLink} to="/" onClick={handleClick}>
          Home
        </Link>,
      ];
    }
  };

  const handleClick = (event) => {
    event.preventDefault();
    console.info('You clicked a breadcrumb.');
    navigate("/");
  };

  const breadcrumbs = generateBreadcrumbs();

  return (
    <Stack spacing={2}>
      <Breadcrumbs separator={<NavigateNextIcon fontSize="small" />} aria-label="breadcrumb">
        {breadcrumbs}
      </Breadcrumbs>
    </Stack>
  );
}

export default CustomSeparator;