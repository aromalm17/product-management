import React from 'react';
import NavBar from '../components/Navbar';
import ProductDetails from '../components/Product/ProductDetails';

const HomePage = () => {
    return (
        <div>
            <NavBar/>
            <ProductDetails />
        </div>
    );
};

export default HomePage;