Product Management Application

This is a Product Management Application built using the MERN (MongoDB, Express, React, Node.js) stack. The application allows users to manage products by adding categories, sub-categories, and detailed product information.

Images : https://drive.google.com/drive/folders/1SSaWnqVBLJeY5pfRjcFFiKrZvP9LBlsT?usp=share_link

- User Authentication: Secure signup and login using JWT.
- Category Management: Add and manage product categories and sub-categories.
- Product Management: Add, edit, and display products with details like name, code, partNumber, images, price, storage, ram, quantity, description.
- Product Images: Upload product images.
- Search and Filter: Search products by name and filter by sub-category.
- Pagination: Navigate through products efficiently.

Once the frontend and backend are connected, data access is only permitted through authenticated routes, ensuring secure communication between the client and server.