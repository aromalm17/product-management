const Product = require('../models/Product');

// Add product
async function addProduct(req, res) {
    try {
        const { name, code, partNumber, subCategory, price, ram, storage, description, quantity } = req.body;
        const images = req.files.map(file => file.path);
        const product = new Product({  name, code,  partNumber,  subCategory,  price,  ram,  storage, description, images, quantity });
        await product.save();
        res.status(201).json({ message: 'Product added successfully', product });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

// Get all products
async function getProducts(req, res) {
    const { page = 1, limit = 8, search = '', manufacturers } = req.query;
    try {
        const query = {
            name: { $regex: search, $options: 'i' },
            ...(manufacturers && { subCategory: { $in: manufacturers.split(',') } }),
        };

        const products = await Product.find(query)
            .populate('subCategory')
            .limit(limit * 1)
            .skip((page - 1) * limit)
            .exec();

        const count = await Product.countDocuments(query);

        res.json({ products, totalPages: Math.ceil(count / limit), currentPage: page });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}


// Get product by id
async function getProductById(req, res) {
    const { id } = req.params;
    try {
        const product = await Product.findById(id).populate('subCategory');
        if (!product) return res.status(404).json({ message: 'Product not found' });
        res.json(product);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

// Update Product
async function updateProduct(req, res) {
    const { id } = req.params;
    const { name, code, brand, partNumber,subCategory, price, storage, ram, description, quantity } = req.body;
    const images = req.files.map(file => file.path);
    try {
        const product = await Product.findByIdAndUpdate(id, { name, code, brand, partNumber, subCategory, price, storage, ram, description, images, quantity }, { new: true });
        if (!product) return res.status(404).json({ message: 'Product not found' });
        res.json({ message: 'Product updated successfully', product });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

module.exports = { addProduct, getProducts, getProductById, updateProduct };