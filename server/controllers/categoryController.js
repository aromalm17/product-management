const Category = require('../models/Category');

// Add category
async function addCategory(req, res) {
    const { name } = req.body;
    try {
        const category = new Category({ name });
        await category.save();
        res.status(201).json({ message: 'Category added successfully' });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Get Catgoery List
async function viewCategories(req, res) {
    try {
        const categories = await Category.find();
        res.status(200).json(categories);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
}

module.exports = { addCategory, viewCategories };