const SubCategory = require('../models/SubCategory');

// Add Subcategory
async function addSubCategory(req, res) {
    const { name, category } = req.body;
    try {
        const subCategory = new SubCategory({ name, category });
        await subCategory.save();
        res.status(201).json({ message: 'Sub-Category added successfully' });
    } catch (error) {
        console.error('Error adding subcategory:', error); 
        res.status(500).json({ error: error.message });
    }
};

// View Subcategory
async function viewSubCategories(req, res) {
    try {
        const subCategories = await SubCategory.find().populate('category');
        res.status(200).json(subCategories);
    } catch (error) {
        console.error('Error retrieving subcategories:', error);
        res.status(500).json({ error: 'Failed to retrieve subcategories. Please try again later.' });
    }
}


module.exports = { addSubCategory, viewSubCategories }