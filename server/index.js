const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT;

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// MongoDB Connection
mongoose.connect(process.env.MONGO_URI)
    .then(() => console.log('MongoDB connected'))
    .catch(err => console.log(err));

// Routes
const authRoutes = require('./routes/authRoute');
const categoryRoutes = require('./routes/categoryRoute');
const subCategoryRoutes = require('./routes/subCategoryRoute');
const productRoutes = require('./routes/productRoute');

// Static Folder
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

// Use Routes
app.use('/user', authRoutes);
app.use('/category', categoryRoutes);
app.use('/subcategory', subCategoryRoutes);
app.use('/product', productRoutes);


app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
