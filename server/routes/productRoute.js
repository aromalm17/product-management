const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../middleware/authMiddleware');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        const originalName = file.originalname.replace(/\s+/g, '_');
        cb(null, Date.now() + '-' + originalName);
    }
});

const upload = multer({ storage: storage });

router.post('/add',auth, upload.array('images', 5), productController.addProduct);
router.get('/',auth, productController.getProducts);
router.get('/:id',auth, productController.getProductById);
router.put('/update/:id',auth, upload.array('images', 5), productController.updateProduct);

module.exports = router;