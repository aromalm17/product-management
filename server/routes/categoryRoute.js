const express = require('express');
const router = express.Router();
const categoryController = require('../controllers/categoryController');
const auth = require('../middleware/authMiddleware');

router.post('/add',auth, categoryController.addCategory);
router.get('/view',auth, categoryController.viewCategories);

module.exports = router;