const express = require('express');
const router = express.Router();
const subCategoryController = require('../controllers/subCategoryController');
const auth = require('../middleware/authMiddleware');

router.post('/add',auth, subCategoryController.addSubCategory);
router.get('/view',auth, subCategoryController.viewSubCategories);

module.exports = router;